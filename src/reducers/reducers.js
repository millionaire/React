import { combineReducers } from 'redux';
import {
  ADD_TODO,
  TOGGLE_TODO,
  SET_VISIBILITY_FILTER,
  VisibilityFilters,
} from '../actions/actions';
import {REQUEST_POSTS, RECEIVE_POSTS} from '../actions/asyncActions';

const { SHOW_ALL } = VisibilityFilters

function visibilityFilter(state = SHOW_ALL, action) {
  switch (action.type) {
    case SET_VISIBILITY_FILTER:
      return action.filter
    default:
      return state
  }
}

function todos(state = [], action) {
  switch (action.type) {
    case ADD_TODO:
      return [
        ...state,
        {
          text: action.text,
          completed: false
        }
      ]
    case TOGGLE_TODO:
      return state.map((todo, index) => {
        if (index === action.index) {
          return Object.assign({}, todo, {
            completed: !todo.completed
          })
        }
        return todo
      })
    default:
      return state
  }
}

const initialState = {
    ip: "2",
    isFetching: false
}
function posts(state = initialState, action){
  switch (action.type){
    case REQUEST_POSTS:
      return Object.assign({}, state,{
        isFetching: true
      })
    case RECEIVE_POSTS: {
      
      return {...state, isFetching:false, ip:action.ip}
    }
    default:
      return state
  }
}


const todoApp = combineReducers({
  posts,
  visibilityFilter,
  todos
})

export default todoApp