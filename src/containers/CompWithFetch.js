import React, { Component } from 'react'
import { connect } from 'react-redux'
import Admin from '../components/Admin'
import AdminWriter from '../components/AdminWriter'
import fetchPosts from '../actions/asyncActions'

class CompWithFetch extends Component {
  render() {
    const { ip, adminIp } = this.props;
    
    return (
      <div>
        <Admin adminIp={adminIp}/>
        <AdminWriter ip={ip} />
      </div>
    )
  }
}

const mapStateToProps = ({posts:{ip}}) => {
 return {
  ip
 }
}

const mapDispatchToProps = (dispatch) => {
      console.log('asdasd')

    return {
        adminIp: () => dispatch(fetchPosts())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CompWithFetch)