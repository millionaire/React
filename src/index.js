import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { Switch, Route } from 'react-router-dom';
import thunkMiddleware from 'redux-thunk';
import './index.css';
import {routes} from './routes';
import App from './App';

import { createStore, applyMiddleware } from 'redux';
import todoApp from './reducers/reducers';

let store = createStore(todoApp, applyMiddleware(thunkMiddleware));

render((
	<Provider store={store}>
    <BrowserRouter>
      <div>
      <App/>
       {routes}
      </div>
  	</BrowserRouter>
  	</Provider>),
  	document.getElementById('root')
);

