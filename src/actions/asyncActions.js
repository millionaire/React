import fetch from 'cross-fetch';

export const REQUEST_POSTS = 'REQUEST_POSTS'
export const RECEIVE_POSTS = 'RECEIVE_POSTS'

const requestPosts = () => ({
	type: REQUEST_POSTS
})

const receivePosts = (ip) => ({
    type: RECEIVE_POSTS,
    ip
})

export default function fetchPosts () {
	return dispatch => {
		dispatch(requestPosts())
      	

		return fetch('http://ip.jsontest.com/')
			.then(data => data.json())
			.then(({ip}) => dispatch(receivePosts(ip)))
	}
}


